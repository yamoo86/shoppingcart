import axios from "axios";


    $(function(){

        // click sur le boutton supprimer une annonce
        $('#delete > a').on('click',function(e){
            
                e.preventDefault();
                var href = $(this).attr('href');
                var id =$(this).attr('id');
                $('#myModal').attr('post', href);
                $('#myModal').attr('ids', id);
                $('#myModal').attr('postid', $(this).attr('post'));
                $('#myModal').modal('show');
                
               
            });

            $('.modal-header > .close').on('click',function(){
                 $('#myModal').modal('hide');
            })

        // click sur le boutton annuler la suppression d'une annonce
        $('.modal-footer > .annuler').on('click',function(){
             $('#myModal').modal('hide');
        });

        // confirmation et suppréssion définitive
        $('.modal-footer > .confirm-delete').on('click',function(){
            var url  = $('#myModal').attr('post');
            $('#myModal').modal('hide');
            axios.get(url).then(function(response){
            alertify.set('notifier','position', 'top-right');
            alertify.set('notifier','delay', 10);
            alertify.success("Suppréssion réussie");
            location.replace(document.referrer);

            
           
            })
             
        });

      

        $('#detail-post a').on('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            axios.get(url).then(function(response){
                console.log(response);
                var shoppin_number = $('#navbarSupportedContent .navbar-nav .shopping-cartes a span');
                var text = parseInt(shoppin_number.text());
                var car = text + 1;
                shoppin_number.text(car);

            })
        })


         $('.add a').on('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            axios.get(url).then(function(response){
                window.location.reload();

            })
        })


         $('.minus a').on('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            axios.get(url).then(function(response){
                window.location.reload();

            })
        })

        $('.cart-delete a').on('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            axios.get(url).then(function(response){
                window.location.reload();

            })
        })

       
    })



        