/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';


// start the Stimulus application
import './bootstrap';

    function onClickBtnLike(event){
        event.preventDefault();
        const url = this.href;
        const spanCount = this.querySelector('span.js-likes');
        const icone = this.querySelector('i');
        axios.get(url).then(function(response){
            const likes = response.data.likes;
            spanCount.textContent = likes;
            if(icone.classList.contains('fas')){
                icone.classList.replace('fas' , 'far');
            } else{
                icone.classList.replace('far' , 'fas');
            }
           
            console.log(response); 
        }).catch(function(error){
            if(error.response.status == 403){
                window.alert("il faut être connecté pour aimer un article");
            }else{
               window.alert("Une erreur s'est produite rézssayer plus tard"); 
            }
        })
    }
   

        $(document).ready(function()
        {
        
        
        document.querySelectorAll('a.js-like').forEach(function(link){
        link.addEventListener('click',onClickBtnLike);
    })
        });


    
        
        function modifier(id)
        {

            $('#myModalmodifier').modal('show');
            var marque = $('#' + id + '> td.marquev').text();
            var model = $('#' + id + '> td.modelv').text();
            var matricule = $('#' + id + '> .matriculev').text();
            var proprietaire = $('#' + id + '> .proprietairev').text();
            document.getElementById('marque').value = marque.trim();
            document.getElementById('model').value = model.trim();
            document.getElementById('matricule').value = matricule.trim();
            document.getElementById('proprietaire').value = proprietaire.trim();
            $('#myModalmodifier').attr('idmodiervoiture', id);

        }

        function okmodifier()
        {
            $("#myModalmodifier").modal('hide');
            id = $('#myModalmodifier').attr('idmodiervoiture');
            marque = $('#marque').val();
            model = $('#model').val();
            matricule = $('#matricule').val();
            proprietaire = $('#proprietaire option:selected').attr('value');
            proprietairetext = $('#proprietaire option:selected').text();
            var arr = proprietairetext.split('->');
            proprietairetext = arr[1].trim();
           
        }
   
   
  

        

