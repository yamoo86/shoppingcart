<?php

namespace App\Cart;

class CartModel
{
    public int $id;
    public string $titre;
    public int $quantity;
    public int $price;
    public int $total;
    public string $image;
    public string $description;


    public function __construct(int $id, string $titre, int $quantity, int $price, string $image, string $description)
    {
        $this->id = $id;
        $this->titre = $titre;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->total = $this->quantity * $this->price;
        $this->image = $image;
        $this->description = $description;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity()
    {
        $this->quantity += 1;
        return $this->quantity;
    }

    public function setPrix($prix)
    {
        $this->price = $prix;
        return $this->price;
    }

    public function setTotal()
    {
        $this->total = $this->quantity * $this->price;
        return $this->total;
    }
}
