<?php

namespace App\Cart;

use App\Entity\Post;
use Symfony\Component\HttpFoundation\RequestStack;


class CartService
{

    public $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }


    public  function checkIfElementAlreadyExits($id, $data): bool
    {
        $recherche = false;

        foreach ($data as $element) {

            foreach ($element as $elt) {
                $key = array_search($elt, $element);
                if ($element[$key] == $id) {
                    $recherche = true;
                    break;
                }
            }
        }

        return $recherche;
    }


    public  function checkArrayKey($id, $data)
    {

        foreach ($data as $element) {

            foreach ($element as $elt) {
                $key = array_search($elt, $element);
                if ($element[$key] == $id) {
                    return $element;
                }
            }
        }
    }



    public function  add(Post $item, $id)
    {

        $cart = [];
        $session =  $this->requestStack->getSession();
        $cart = $session->get('cart');
        if ($cart == null) {
            $carmodel = ['id' => $item->getId(), 'titre' => $item->getTitre(), 'quantity' => 1, 'price' => $item->getPrix(), 'totalPrice' => $item->getPrix(), 'image' => $item->getImage(), 'description' => $item->getContent()];
            $tab_cart = [];
            array_push($tab_cart, $carmodel);
            $qte = 1;
            $session->set('totalQuantity', $qte);
            $session->set('totalPrice', $item->getPrix());
            $session->set('cart', $tab_cart);
        } else {

            $cardExist = $this->checkIfElementAlreadyExits($id, $cart);
            if ($cardExist == true) {

                $produit = $this->checkArrayKey($id, $cart);
                $new_array = \array_filter($cart, static function ($element) use ($id) {
                    return $element['id'] !== $id;
                });

                $produit['quantity'] += 1;
                $produit['totalPrice'] = $produit['quantity'] * $produit['price'];
                array_push($new_array, $produit);
                $qte = $session->get('totalQuantity');
                $qte += 1;
                $new_price = $session->get('totalPrice') + $produit['price'];
                $session->set('totalQuantity', $qte);
                $session->set('totalPrice', $new_price);
                $session->set('cart', $new_array);
            } else {

                $carmodel =
                    ['id' => $item->getId(), 'titre' => $item->getTitre(), 'quantity' => 1, 'price' => $item->getPrix(), 'totalPrice' => $item->getPrix(), 'image' => $item->getImage(), 'description' => $item->getContent()];
                $qte = $session->get('totalQuantity');
                $qte += 1;
                $session->set('totalQuantity', $qte);
                $new_price = $session->get('totalPrice') + $item->getPrix();
                $session->set('totalPrice', $new_price);
                array_push($cart, $carmodel);
                $session->set('cart', $cart);
            }
        }
    }


    public function reduceByOne($id)
    {


        $session =  $this->requestStack->getSession();
        $cart = $session->get('cart');
        $produit = $this->checkArrayKey($id, $cart);
        $produit['quantity']--;

        $new_array = \array_filter($cart, static function ($element) use ($id) {
            return $element['id'] !== $id;
        });

        if ($produit['quantity'] == 0) {
            $qte = $session->get('totalQuantity');
            $qte -= 1;
            $produit['totalPrice'] = $produit['quantity'] * $produit['price'];
            $new_price = $session->get('totalPrice') - $produit['price'];
            $session->set('totalPrice', $new_price);
            $session->set('totalQuantity', $qte);
            $session->set('cart', $new_array);
        } else {
            $qte = $session->get('totalQuantity');
            $qte -= 1;
            $produit['totalPrice'] = $produit['quantity'] * $produit['price'];
            $new_price = $session->get('totalPrice') - $produit['price'];
            $session->set('totalPrice', $new_price);
            array_push($new_array, $produit);
            $session->set('totalQuantity', $qte);
            $session->set('cart', $new_array);
        }
    }



    public function addOne($id)
    {

        $session =  $this->requestStack->getSession();
        $cart = $session->get('cart');
        $produit = $this->checkArrayKey($id, $cart);
        $produit['quantity']++;

        $new_array = \array_filter($cart, static function ($element) use ($id) {
            return $element['id'] !== $id;
        });

        $qte = $session->get('totalQuantity');
        $qte += 1;
        $produit['totalPrice'] = $produit['quantity'] * $produit['price'];
        $new_price = $session->get('totalPrice') + $produit['price'];
        $session->set('totalPrice', $new_price);
        array_push($new_array, $produit);
        $session->set('totalQuantity', $qte);
        $session->set('cart', $new_array);
    }


    public function removeItem($id)
    {
        $session =  $this->requestStack->getSession();
        $cart = $session->get('cart');

        $new_array = \array_filter($cart, static function ($element) use ($id) {
            return $element['id'] !== $id;
        });
        $produit = $this->checkArrayKey($id, $cart);
        $qte = $session->get('totalQuantity');
        $newQte = $qte - $produit['quantity'];
        $produit['totalPrice'] = $produit['quantity'] * $produit['price'];
        $new_price = $session->get('totalPrice') - $produit['totalPrice'];
        $session->set('totalPrice', $new_price);
        $session->set('totalQuantity', $newQte);
        $session->set('cart', $new_array);
    }




    public function generateArray($data)
    {
        $response = [];
        foreach ($data as $key => $element) {
            array_push($response, $element);
        }

        return $response;
    }
}
