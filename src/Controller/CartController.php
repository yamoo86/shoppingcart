<?php

namespace App\Controller;

use App\Cart\CartService;
use App\Entity\Post;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;


class CartController extends AbstractController
{

    public function __construct(private PostRepository $postRepository, private EntityManagerInterface $em, private CartService $cartService, private RequestStack $requestStack)
    {
    }


    #[Route('/add/cart/{id}', name: 'app_add_cart')]
    /**
     * addCard
     * Permet d'ajouter un produit au panier
     * @param  mixed $post
     * @return void
     */
    public function addCard(Post $post, Request $request)
    {
        if ($post == null) {
            // return $this->redirectToRoute($request->attributes->get('_route'), [
            //     'id' => $post->getId()
            // ]);
            return $this->redirectToRoute('app_post_get');
        }

        $this->cartService->add($post, $post->getId());
        // return $this->redirectToRoute($request->attributes->get('_route'), [
        //     'id' => $post->getId()
        // ]);
        return $this->json(["message" => "success"], 200);
        //return $this->redirectToRoute('app_post');
    }

    #[Route('/reduce/cart/{id}', name: 'app_cart_reduce')]
    /**
     * reduceCart
     * Enlève un produit du panier
     * @param  mixed $post
     * @return void
     */
    public function reduceCart(Post $post, Request $request)
    {

        $this->cartService->reduceByOne($post->getId());

        return $this->json(["message" => "success"], 200);
    }


    #[Route('/add/to/cart/{id}', name: 'app_add_one_to_cart')]
    /**
     * reduceCart
     * Enlève un produit du panier
     * @param  mixed $post
     * @return void
     */
    public function addOneToCart(Request $request, Post $post)
    {

        $this->cartService->addOne($post->getId());

        return $this->json(["message" => "success"], 200);
    }


    #[Route('/{_locale}/display/cart', name: 'display_cart')]
    /**
     * getCart
     * Affiche un panier et son contenu
     * @return void
     */
    public function getCart(Request $request)
    {
        $session = $request->getSession();
        if (!$session->get('cart')) {
            return $this->render('cart/cart.html.twig', ['products' => null]);
        }
        $cart = $session->get('cart');

        $totalPrice = $session->get('totalQuantity');
        $cartService = new CartService($this->requestStack);

        return $this->render('cart/cart.html.twig', ['products' => $cartService->generateArray($cart), 'totalPrice' => $totalPrice]);
    }

    #[Route('/delete/to/cart/{id}', name: 'delete_to_cart')]
    /**
     * getCart
     * Supprimer un produit du panier
     * @return void
     */
    public function deleteToCart(Request $request)
    {
        $session = $request->getSession();
        if (!$session->get('cart')) {
            return $this->render('cart/cart.html.twig', ['products' => null]);
        }
        $cart = $session->get('cart');

        $totalPrice = $session->get('totalQuantity');
        $cartService = new CartService($this->requestStack);


        return $this->render('cart/cart.html.twig', ['products' => $cartService->generateArray($cart), 'totalPrice' => $totalPrice]);
    }


    #[Route('/remove/to/cart/{id}', name: 'remove_to_cart')]
    /**
     * Supprimer un produit completement du panier
     * @return void
     */
    public function removeItemToCart(Post $post)
    {
        $this->cartService->removeItem($post->getId());

        return $this->json(["message" => "success"], 200);
    }



    #[Route('/{_locale}/checkout/cart', name: 'ckeckout_cart')]
    /**
     * checkout
     * Page de paiement d'un produit par carte bancaire
     * @return void
     */
    public function checkout()
    {
        $session = $this->requestStack->getSession();
        // if (!$session->get('cart')) {
        //     return response . redirect('/shopping-cart');
        // }

        // $errMsg = $session . flashMessages->get('error');
        // $this->addFlash('success', 'Article Created! Knowledge is power!');
        return $this->render('cart/checkout.html.twig');
    }
}
