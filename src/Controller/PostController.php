<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Service\RandomDataService;
use App\Service\UploadFileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Api\Api;
use App\Entity\PostLike;
use App\Repository\PostLikeRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;



class PostController extends AbstractController
{

    public function __construct(private TranslatorInterface $translator, private RandomDataService $randomDataService, private UploadFileService $fileUploader, private EntityManagerInterface $em, private PostRepository $postRepository, private PostLikeRepository $postLikeRepository)
    {
    }

    #[Route('/{_locale}/post', name: 'app_post')]
    #[Route('/{_locale}/post/edit/{id}', name: 'app_post_edit')]
    public function index(Post $post = null, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if (!$post) {
            $post = new Post();
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            if (!$post->getId()) {
                $post->setCreatedAt(new \DateTimeImmutable());
            }

            $brochureFile = $form->get('image')->getData();
            $targetDirectory = $this->getParameter('images_posts_directory');
            $post->setUsers($this->getUser());
            $post->setLabel($this->randomDataService->getLabel());
            $post->setTag($this->randomDataService->getTag());
            $post->setContent($form->get('content')->getData());
            $post->setTitre($form->get('titre')->getData());
            $post->setPrix($form->get('prix')->getData());

            $fichier = random_int(170, 695893) . uniqid(rand(), true) . $brochureFile->getClientOriginalName();

            $brochureFile->move(
                $targetDirectory,
                $fichier
            );
            $post->setImage($fichier);
            $this->em->persist($post);
            $this->em->flush();

            $this->addFlash('success', $this->translator->trans(
                'article_created_flash'
            ));

            return $this->redirectToRoute('app_post_get');
        }

        // $posts = $this->postRepository->findAll();

        return $this->renderForm('post/index.html.twig', [
            'form' => $form,
            'update' => $post->getId() !== null
        ]);
    }







    #[Route('/{_locale}/post/{id}', name: 'detail_post')]
    public function detailPost(Request $request, Post $post)
    {

        $api = new Api();
        $url = $api->apiUrl() . "uploads/posts/";

        if ($post) {
            return $this->render('post/post_detail.html.twig', [
                'post' => $post,
                'url' => $url
            ]);
        }
    }




    #[Route('/{_locale}/delete/post/{id}', name: 'delete_post')]
    public function deletePost(Request $request, Post $post)
    {

        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // $this->denyAccessUnlessGranted('POST_DELETE', $post);
        $this->em->remove($post);
        $this->em->flush();
        return $this->redirectToRoute('app_post');
    }





    /**
     * @Route("/{_locale}/posts/page/{page<[1-9]\d*>}",defaults={"page": "1"}, methods="GET", name="app_post_get")
     *
     * @param Request $request
     * @param integer $page
     * @return void
     */
    public function recupPost(Request $request, int $page, PaginatorInterface $paginator)
    {


        $posts = $this->postRepository->findLatest($page);



        $api = new Api();
        $url = $api->apiUrl() . "uploads/posts/";
        return $this->renderForm('post/post.html.twig', [
            'paginator' => $posts,
            'url' => $url
        ]);
    }





    #[Route('/{_locale}/post/{id}/like', name: 'like_post')]
    /**
     * like
     *
     * Cette fonction permet d'aimer ou ne plus aimer un post
     * @param  mixed $post
     * @return void
     */
    public function like(Request $request, Post $post)
    {
        $user = $this->getUser();
        if (!$user)
            return $this->json(["message" => "il faut être connecté"], 403);

        if ($post->isLikedByUser($user)) {
            $like = $this->postLikeRepository->findOneBy([
                'post' => $post,
                'users' => $user

            ]);

            $this->em->remove($like);
            $this->em->flush();
            return $this->json(["message" => "success", "likes" => $this->postLikeRepository->count(['post' => $post])], 200);
        } else {

            $like = new PostLike();
            $like->setPost($post);
            $like->setUsers($user);
            $this->em->persist($like);
            $this->em->flush();
            return $this->json(["message" => "success", "likes" => $this->postLikeRepository->count(['post' => $post])], 200);
        }
    }
}
