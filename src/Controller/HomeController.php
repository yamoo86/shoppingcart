<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class HomeController extends AbstractController
{
    #[Route('/{_locale}/', name: 'app_home')]
    public function index(Request $request): Response
    {

        $session = $request->getSession();
        if ($session->get('totalQuantity') == null) {
            $session->set('totalQuantity', 0);
        }

        if ($session->get('totalPrice') == null) {
            $session->set('totalPrice', 0);
        }
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }



    #[Route('/{_locale}/', name: 'app_changer_langue')]
    public function changeDeLangue(Request $request): Response
    {



        $request->setLocale($request->getLocale());
        return $this->redirect($request->request->get('referer'));
        //return $this->redirectToRoute($request->attributes->get('_route'));
    }



    #[Route('/{_locale}/contact', name: 'app_contact')]
    public function contact(): Response
    {
        return $this->render('home/contact.html.twig');
    }

    #[Route('/download/cv', name: 'app_cv')]
    public function telechargerCv()
    {


        $pdfPath = $this->getParameter('cv_greg') . '/gregoire_cv.pdf';

        return $this->file($pdfPath);
    }
}
