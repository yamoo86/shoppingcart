<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\UX\Dropzone\Form\DropzoneType;

class PostType extends AbstractType
{

    public function __construct(private TranslatorInterface $translator)
    {
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => $this->translator->trans('titre_publication'),
                'constraints' => [new Length(['min' => 5, 'minMessage' => $this->translator->trans('titre_minmessage'), 'maxMessage' => $this->translator->trans('titre_maxmessage')])],
            ])
            ->add('prix', IntegerType::class, [
                'label' => $this->translator->trans('prix_publication'),
                'constraints' => [new Length(['min' => 2, 'minMessage' => $this->translator->trans('minmessage_prix_publication'), 'max' => 4, 'maxMessage' => $this->translator->trans('maxmessage_prix_publication')])],
            ])
            ->add('content', TextareaType::class, [
                'label' => $this->translator->trans('contenu_publication'),
                'constraints' => [new Length(['min' => 6, 'minMessage' => $this->translator->trans('minmessage_contenu_publication')])],

            ])
            ->add('image', DropzoneType::class, [
                'label' =>
                $this->translator->trans('image_principale'),

                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpg',
                            'image/png',
                            'image/jpeg',
                            'image/JPEG',
                            'image/PNG',
                            'image/JPG'
                        ],
                        'mimeTypesMessage' => $this->translator->trans(
                            'mimeType_Message'
                        ),
                        'uploadFormSizeErrorMessage' => $this->translator->trans(
                            'uploaSize_error_Message'
                        ),
                    ])
                ],
                'attr' => [
                    'placeholder' => $this->translator->trans('image_placeholder'),
                    'accept' => '.jpg, .jpeg, .png, .PNG, .JPG, .JPEG'
                ],

                'data_class' => null


            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
