<?php

namespace App\Service;

use App\Entity\Option;
use App\Repository\PostRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PostService
{
    public function __construct(
        private RequestStack $requestStack,
        private PostRepository $postRepository,
        private PaginatorInterface $paginator
    ) {
    }

    public function getPaginatedPost(): PaginationInterface
    {
        $request = $this->requestStack->getMainRequest();
        $articlesQuery = $this->articleRepo->findForPagination();
        $page = $request->query->getInt('page', 1);
        $limit = 10;

        return $this->paginator->paginate($articlesQuery, $page, $limit);
    }
}
