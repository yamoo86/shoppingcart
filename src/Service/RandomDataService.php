<?php

namespace App\Service;

class RandomDataService
{
    const POSTS_COLOUR = ["teal", "purple", "pink", "red", "yellow", "violet", "green", "beau", "calm", "marron"];
    const POSTS_TAG = ["Informatique", "Chimie", "Mathematique", "Cuisine", "Sport", "Voyage", "Decouverte", "Visite", "Travail", "Innovation"];

    public function getLabel()
    {
        $key = array_rand(self::POSTS_COLOUR);
        return self::POSTS_COLOUR[$key];
    }

    public function getTag()
    {
        $key = array_rand(self::POSTS_TAG);
        return self::POSTS_TAG[$key];
    }
}
