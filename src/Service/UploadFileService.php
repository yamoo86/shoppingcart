<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadFileService
{
    public $fichier;

    public function upload(UploadedFile $file, string $targetDirectory): string
    {
        $fichier = random_int(170, 695893) . uniqid(rand(), true) . $file->getClientOriginalName();

        try {
            $file->move($targetDirectory, $fichier);
            return
                $fichier;
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }
    }

    public function getCurrentFile()
    {
        return $this->fichier;
    }
}
